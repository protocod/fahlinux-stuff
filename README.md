# fahlinux stuff

## Systemd service
inspired by https://gist.github.com/lopezpdvn/81397197ffead57c2e98

You can find a sample a systemd service for the official FAHClient. `fahclient.service`

To add it on your system, ensure you to have good rights

```sh
sudo chown root:root fahclient.service
sudo chmod u=rw,go=r fahclient.service
```

Then, add the service using...
```sh
sudo mv fahclient.service /etc/systemd/system
sudo systemctl daemon-reload
```

You can start or stop the service like this
```sh
sudo systemctl start fahclient.service
sudo systemctl stop fahclient.service
```
